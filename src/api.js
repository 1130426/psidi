
const conf = require('./conf');
const funcs = require('./funcs');
const fs = require('fs');

const http = require('http');

const api = conf.apiURL;
const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const jwt = require('jsonwebtoken');
const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy
const GoogleStrategy = require('passport-google-oauth20').Strategy;

const util = require('util')
const request = require("request");
const requestPromise = util.promisify(request);

const productsService = conf.productsURL;
const usersService = conf.usersURL;
const reviewsService = conf.reviewsURL;

const secret = 'secret';
app.use(cookieParser());
app.use(passport.initialize());

// expiry in seconds or duration strings
const JWT_EXPIRATION = '1h';
// const JWT_EXPIRATION = 30; // seconds
var jwtOpts = {
    jwtFromRequest: (req) => {
        var token = null;
        if (req && req.cookies) {
            token = req.cookies['jwt']
        }
        return token
    },
    secretOrKey: secret
};

function checkUser(user) {

    console.log(user);
    console.log(" tried to log in");

    return requestPromise({
        uri: usersService + "/" + user.userID,
        qs: user.password ? { password: user.password } : { provider: user.provider },
        json: true
    });
}

// main authentication, our app will rely on it
passport.use(new JwtStrategy(jwtOpts, async function (jwt_payload, done) {
    // only executes if token exists and has not expired
    return done(null, jwt_payload.data)
}));

const auth_middleware = function (req, res, next) {
    passport.authenticate('jwt', function (err, user, info) {
        req.user = user;
        next();
    })(req, res, next);
};

const GOOGLE_CLIENT_ID = fs.readFileSync(__dirname + '/oauth/id', 'utf8');
const GOOGLE_CLIENT_SECRET = fs.readFileSync(__dirname + '/oauth/secret', 'utf8');
passport.use(
    new GoogleStrategy({
        clientID: GOOGLE_CLIENT_ID,
        clientSecret: GOOGLE_CLIENT_SECRET,
        callbackURL: api + "/auth/google/callback"
    },
        function (accessToken, refreshToken, profile, done) {
            return done(null, profile);
        }
    )
);

/** 
 * @swagger
 * /auth/basic:
 *   post:
 *     tags:
 *     - Auth
 *     summary: Authentication
 *     description: >-
 *       Allows user authentication. If the user is already registered, he can either be a regular User or a Moderator
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               username:
 *                 type: string
 *               password:
 *                 type: string
 *     responses:
 *       '200':
 *         description: Login successful
 *       '400':
 *         description: Invalid login credentials
*/
app.post('/auth/basic', async (req, res) => {
    return await checkUser({ userID: req.body.username, password: req.body.password })
        .then(checkUserResp => {
            if (checkUserResp.statusCode == "200") {

                delete checkUserResp.body.password;

                let token = jwt.sign(
                    { data: checkUserResp.body },
                    'secret',
                    { expiresIn: JWT_EXPIRATION }
                );
                res.cookie('jwt', token);
                res.status(200).send("Login successful");
            } else {
                res.status(400).send('Invalid login credentials');
            }
        })
        .catch((e) => res.status(500).send("Service unavailable."));
});

app.param('userID', function (req, res, next, userID) {
    req.userID = userID;
    return next();
});

/**
 * @swagger
 *
 * /auth/register/{userID}:
 *   put:
 *     tags:
 *     - Auth
 *     summary: Register
 *     description: >-
 *       Allows user registration
 *     parameters:
 *       - $ref: '#/components/parameters/userIDPathParameter'
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               password:
 *                 type: string
 *     responses:
 *       '200':
 *         description: Success
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               items:
 *                 $ref: '#/components/schemas/reviews'
 */
app.put('/auth/register/:userID', (req, res) => {

    // TODO: ADD ETAGS
    request({
        method: "PUT",
        uri: usersService + "/" + req.userID,
        body: req.body,
        json: true,
        headers: { "Original-URL": api + req.originalUrl }
    }, function (err, response, body) {
        if (err) {
            res.status(500).send("Service unavailable.");
            return;
        }

        if (response.statusCode != 201) {
            console.log(body);
            res.status(response.statusCode).send(body);
        } else {
            console.log("Created user: " + req.userID);
            console.log(body);

            delete body.password;

            let token = jwt.sign({
                data: body
            }, 'secret', { expiresIn: JWT_EXPIRATION });

            res.cookie('jwt', token);
            res.status(201).send("User " + req.userID + " registered.");
        }
    });
});

app.get('/auth/google', passport.authenticate('google', { scope: ['profile', 'email'] }));

app.get('/auth/google/success', function (req, res) {
    res.send("Google auth success");
});
app.get('/auth/google/error', function (req, res) {
    res.send("Google auth error");
});

passport.serializeUser(function (user, cb) {
    cb(null, user);
});
passport.deserializeUser(function (obj, cb) {
    cb(null, obj);
});

app.get('/auth/google/callback',
    passport.authenticate('google', { failureRedirect: '/auth/google/error' }),
    async function (req, res) {

        console.log("------------------------");
        console.log("Google auth callback user: ");
        console.log(req.user);

        return await checkUser({ userID: req.user._json.email, provider: req.user.provider })
            .then(checkUserResp => {
                console.log("After promiss resolved");
                console.log(checkUserResp.statusCode);

                if (checkUserResp.statusCode == "200") {
                    // user exists
                    console.log("User " + req.user._json.email + " exists");

                    delete checkUserResp.body.password;
                    console.log(checkUserResp.body);
                    let token = jwt.sign(
                        { data: checkUserResp.body },
                        'secret',
                        { expiresIn: JWT_EXPIRATION }
                    );
                    res.cookie('jwt', token);
                    res.redirect('/auth/google/success');
                } else {
                    // user doesn't exist
                    const user = {
                        displayName: req.user.displayName,
                        name: req.user.name.givenName,
                        userID: req.user._json.email,
                        provider: req.user.provider
                    };

                    request({
                        method: "PUT",
                        uri: usersService + "/" + req.user._json.email,
                        body: user,
                        json: true,
                        headers: { "Original-URL": api + req.originalUrl }
                    }, function (err, response, body) {
                        if (err) {
                            res.status(500).send("Service unavailable.");
                            return;
                        }

                        delete body.password;
                        let token = jwt.sign({
                            data: body
                        }, 'secret', { expiresIn: JWT_EXPIRATION });

                        res.cookie('jwt', token);
                        res.redirect('/auth/google/success');
                    });
                }
            })
            .catch((e) => res.status(500).send("Service unavailable."));
    });


/** 
 * @swagger
 * components:
 *   securitySchemes:
 *     oAuthSec:
 *       type: oauth2
 *       description: This API uses OAuth 2 with the implicit grant flow
 *       flows:
 *         implicit:
 *           authorizationUrl: http://localhost:2999/auth/google
 *           scopes:
 *             mod: gives moderator access
 *             user: gives registered user access
 *   parameters:
 *     reviewIDPathParameter:
 *       name: reviewID
 *       in: path
 *       description: Review id identifier
 *       required: true
 *       schema:
 *         type: string
 *     productIDPathParameter:
 *       name: productID
 *       in: path
 *       description: Product id identifier
 *       required: true
 *       schema:
 *         type: string
 *     ImageIdPathParameter:
 *       name: imageID
 *       in: path
 *       description: Image id identifier
 *       required: true
 *       schema:
 *         type: string
 *     userIDPathParameter:
 *       name: userID
 *       in: path
 *       description: User id identifier
 *       required: true
 *       schema:
 *         type: string
 *   schemas:
 *     reviewActionBody:
 *       type: object
 *       properties:
 *         action:
 *           type: string
 *           enum: [approve, reject, report, vote]
 *     newReviewBody:
 *       description: the body needed to create a new review
 *       properties:
 *         product:
 *           type: string
 *         description:
 *           type: string
 *         rating:
 *           type: string
 *     review:
 *       description: a full review
 *       properties:
 *         id:
 *           type: string
 *         username:
 *           type: string
 *         review:
 *           type: string
 *         productID:
 *           type: string
 *         rating:
 *           type: string
 *         status:
 *           type: string
 *         createdOn:
 *           type: string
 *           format: date-time
 *         updatedOn:
 *           type: string
 *           format: date-time
 *         publishedDate:
 *           type: string
 *           format: date-time
 *     productImages:
 *       description: images of a product
 *       properties:
 *         images:
 *           type: array
 *           items:
 *             type: string
 *     productDetails:
 *       description: details of a product
 *       properties:
 *         images:
 *           type: array
 *           items:
 *             type: string
 *         sku:
 *           type: string
 *     product:
 *       description: full product
 *       properties:
 *         name:
 *           type: string
 *         description:
 *           type: string
 *         details:
 *           $ref: '#/components/schemas/productDetails'
 *     productDTO:
 *       description: partial product
 *       properties:
 *         name:
 *           type: string
 *         description:
 *           type: string
 *     productDTOs_paginated:
 *       description: list of products paginated
 *       properties:
 *         next:
 *           $ref: '#/components/schemas/pagination'
 *         previous:
 *           $ref: '#/components/schemas/pagination'
 *         products:
 *           type: array
 *           items:
 *             $ref: '#/components/schemas/productDTO'
 *     productRating:
 *       description: aggregated rating of a product
 *       properties:
 *         aggregatedRating:
 *           type: number
 *         reviews:
 *           type: number
 *         stars:
 *           type: object
 *           properties:
 *             0:
 *               type: number
 *             0.5:
 *               type: number
 *             1:
 *               type: number
 *             1.5:
 *               type: number
 *             2:
 *               type: number
 *             2.5:
 *               type: number
 *             3:
 *               type: number
 *             3.5:
 *               type: number
 *             4:
 *               type: number
 *             4.5:
 *               type: number
 *             5:
 *               type: number
 *     reviews_paginated:
 *       description: a list of reviews paginated
 *       properties:
 *         next:
 *           $ref: '#/components/schemas/pagination'
 *         previous:
 *           $ref: '#/components/schemas/pagination'
 *         reviews:
 *           type: array
 *           items:
 *             $ref: '#/components/schemas/review'
 *     reviews:
 *       description: a list of reviews
 *       properties:
 *         reviews:
 *           type: array
 *           items:
 *             $ref: '#/components/schemas/review'
 *     pagination:
 *       description: the current pagination
 *       properties:
 *         page:
 *           type: string
 *         limit:
 *           type: string
 */

var swaggerJSDoc = require('swagger-jsdoc');
var swaggerJSOptions = {
    swaggerDefinition: {
        openapi: "3.0.0",
        info: {
            title: 'Psidianos\' Masterpiece',
            version: '1',
            description: "A product and reviews system",

        },
        servers: [{
            url: conf.apiURL
        }]
    },

    apis: [
        __dirname + '/api.js'
    ],
};
var swaggerSpec = swaggerJSDoc(swaggerJSOptions);

// console.log(swaggerSpec);

var swaggerUi = require('swagger-ui-express');

app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

app.get('/openapi.json', function (req, res) {
    res.json(swaggerSpec);
});

app.get('/', function (req, res) {

    var body = {};
    var _links = {}

    funcs.addLink(_links, "self", api);
    funcs.addLink(_links, "describedBy", api + "/openapi.json");
    funcs.addLink(_links, "help", api + "/docs");
    funcs.addLink(_links, conf.linkRelPrefix + ":products", api + "/products");
    funcs.addLink(_links, conf.linkRelPrefix + ":reviews", api + "/reviews");
    funcs.addLink(_links, conf.linkRelPrefix + ":login", [api + "/auth/basic", api + "/auth/google"]);
    funcs.addLink(_links, conf.linkRelPrefix + ":register", api + "/users/{?username}");

    body._links = _links;
    res.status(200).json(body);
});


/**
 * @swagger
 * /reviews:
 *   get:
 *     tags:
 *     - Reviews
 *     summary: Returns all reviews by status
 *     description: >-
 *       Returns all reviews. Optionally filters reviews by status, if provided as a query parameter
 *     parameters:
 *       - name: status
 *         in: query
 *         description: the status of the reviews
 *         schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: Success
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/review'
 *   post:
 *     tags:
 *       - Reviews
 *     summary: Create a new review
 *     description: >-
 *       Creates a review for a given product
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/newReviewBody'
 *     responses:
 *       '202':
 *          description: Accepted
*/
app.route("/reviews")
    .get(auth_middleware, function (req, res) {

        console.log(req.user);
        if (req.user) {
            req.query.authenticatedUser = req.user.userID;
            req.query.roles = req.user.roles.join();
        }

        request({ uri: reviewsService, qs: req.query, headers: { "Original-URL": api + req.originalUrl } }, function (err, resp, body) {
            if (err) {
                res.status(500).send();
            }
        }).pipe(res);
    })
    .post(auth_middleware, function (req, res) {

        if (!req.user) {
            console.log("User not authenticated");
            res.status(401).send("Unauthorized");
            return;
        }

        req.body.user = req.user.userID;

        request({ method: "POST", uri: reviewsService, body: req.body, json: true, headers: { "Original-URL": api + req.originalUrl } }, function (err, resp, body) {
            if (err) {
                res.status(500).send();
            }
        }).pipe(res);
    });


app.param('reviewID', function (req, res, next, reviewID) {
    req.reviewID = reviewID;
    return next();
});

/**
 * @swagger
 * /reviews/{reviewID}:
 *   get:
 *     tags:
 *     - Reviews
 *     summary: Returns a single review
 *     description: >-
 *       Returns a single review
 *     parameters:
 *       - $ref: '#/components/parameters/reviewIDPathParameter'
 *     responses:
 *       '200':
 *         description: Success
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/review'
 *   delete:
 *     tags:
 *       - Reviews
 *     summary: Withdraw a review
 *     description: A registered customer can withdraw one of his reviews if it has no votes.
 *     parameters:
 *       - $ref: '#/components/parameters/reviewIDPathParameter'
 *     responses:
 *       '200':
 *         description: The review has been deleted successfully
 *       '400':
 *         description: Etag out-of-date. Please pull the lastest review and update the etag accordingly.
 *       '401':
 *         description: Unauthorized
 *       '403':
 *         description: You can't delete a review you don't own
 *       '412':
 *         description: The review you are trying to delete has votes already. Therefore, it cannot be deleted.
 *   post:
 *     tags:
 *     - Reviews
 *     summary: Approve/Reject a review
 *     description: >-
 *       Approves or Rejects a review
 *     parameters:
 *       - $ref: '#/components/parameters/reviewIDPathParameter'
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/reviewActionBody'
 *     responses:
 *       '200':
 *         description: Success
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/review'
*/
app.route("/reviews/:reviewID")
    .get(auth_middleware, function (req, res) {

        request({
            uri: reviewsService + "/" + req.reviewID,
            qs: req.user ? { authenticatedUser: req.user.userID, roles: req.user.roles.join() } : null,
            json: true,
            headers: { "Original-URL": api + req.originalUrl }
        },
            function (err, resp, body) {
                if (err) {
                    res.status(500).send();
                }
            }).pipe(res);
    })
    .delete(auth_middleware, function (req, res) {
        console.log(req.user);
        if (req.user) {
            req.query.authenticatedUser = req.user.userID;
            req.query.roles = req.user.roles.join();
        } else {
            console.log("User not authenticated");
            res.status(401).send("Unauthorized");
            return;
        }
        request({
            method: "DELETE",
            uri: reviewsService + "/" + req.reviewID,
            body: req.body,
            qs: req.query,
            headers: { "if-match": req.headers["if-match"], "Original-URL": api + req.originalUrl },
            json: true
        },
            function (err, resp, body) {
                if (err) {
                    res.status(500).send();
                }
            }).pipe(res);
    })
    .post(auth_middleware, function (req, res) {
        console.log(req.user);
        if (req.user) {
            req.query.authenticatedUser = req.user.userID;
            req.query.roles = req.user.roles.join();
        } else {
            console.log("User not authenticated");
            res.status(401).send("Unauthorized");
            return;
        }
        request({
            method: "POST",
            uri: reviewsService + "/" + req.reviewID,
            body: req.body,
            qs: req.query,
            headers: { "if-match": req.headers["if-match"], "Original-URL": api + req.originalUrl },
            json: true
        },
            function (err, resp, body) {
                if (err) {
                    res.status(500).send();
                }
            }).pipe(res);
    });


// URL: /products
//
// GET 		return all products with pagination
// pagination params:
// - limit -> a number
// - page -> a number
//
// other params:
// - barcode -> a number
// - productID -> an ID
//
// Ex: GET /products?page=2&limit=10&productID=abc&barcode=12345
//

/**
 * @swagger
 * /products:
 *   get:
 *     tags:
 *     - Products
 *     summary: Returns all products with pagination
 *     description: >-
 *       Returns all products. Optionally allows pagination as a query parameter
 *     parameters:
 *       - name: limit
 *         in: query
 *         description: the page size
 *         schema:
 *           type: string
 *       - name: page
 *         in: query
 *         description: the current page
 *         schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: Success
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/productDTOs_paginated' 
*/
app.route("/products")
    .get(function (req, res) {

        request({ uri: productsService, qs: req.query, headers: { "Original-URL": api + req.originalUrl } },
            function (err, resp, body) {
                if (err) {
                    res.status(500).send();
                }
            }).pipe(res);
    });

app.param('productID', function (req, res, next, productID) {
    req.productID = productID;
    return next();
});

/**
 * @swagger
 * /products/{productID}:
 *  get:
 *    tags:
 *    - Products
 *    summary: Product details
 *    description: >-
 *      Returns the product details
 *    parameters:
 *      - $ref: '#/components/parameters/productIDPathParameter'
 *    responses:
 *      '200':
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/product'
 *      '404':
 *        description: Product not found
*/
app.route("/products/:productID")
    .get(function (req, res) {
        request({
            uri: productsService + "/" + req.productID,
            qs: req.query,
            json: true,
            headers: { "Original-URL": api + req.originalUrl }
        },
            function (err, resp, body) {
                if (err) {
                    res.status(500).send();
                }
            }).pipe(res);
    });

// URL: /products/{productID}/sku
//
// GET 	return the SKU for this product
//
// Ex: GET /products/25/sku
//

/**
 * @swagger
 * /products/{productID}/sku:
 *   get:
 *     tags:
 *     - Products
 *     summary: SKU of a product
 *     description: >-
 *       Returns the sku of the given product
 *     parameters:
 *       - $ref: '#/components/parameters/productIDPathParameter'
 *     responses:
 *       '200':
 *         description: Success
 *         content:
 *           image/png:
 *             schema:
 *               type: string
 *               format: binary
*/
app.route("/products/:productID/sku")
    .get(function (req, res) {
        request({ uri: productsService + "/" + req.productID + "/sku", headers: { "Original-URL": api + req.originalUrl } }, function (err, resp, body) {
            if (err) {
                res.status(500).send();
            }
        }).pipe(res);
    });

app.param('imageID', function (req, res, next, imageID) {
    req.imageID = imageID;
    return next();
});

/**
 * @swagger
 * /products/{productID}/images/{imageID}:
 *   get:
 *     tags:
 *     - Products
 *     summary: Returns an image of a product
 *     description: >-
 *       Returns an image of a product
 *     parameters:
 *       - $ref: '#/components/parameters/productIDPathParameter'
 *       - $ref: '#/components/parameters/ImageIdPathParameter'
 *     responses:
 *       '200':
 *         description: Success
 *         content:
 *           image/png:
 *             schema:
 *               type: string
 *               format: binary
 */
app.route("/products/:productID/images/:imageID")
    .get(function (req, res) {
        request({
            uri: productsService + "/" + req.productID + "/images/" + req.imageID,
            headers: { "Original-URL": api + req.originalUrl }
        },
            function (err, resp, body) {
                if (err) {
                    res.status(500).send();
                }
            }).pipe(res);
    });

// URL: /products/:productID/reviews
//
// GET 		return all published reviews for a product, with pagination
// pagination params:
// - limit -> a number
// - page -> a number
// Ex: GET /products/25/reviews?page=2&limit=10
//
// POST 	posts a new review for a given product
// 
// Ex: POST 
//      {	
//          description: x
//          rating: y
//      }
//

/**
 * @swagger
 * /products/{productID}/reviews:
 *   get:
 *     tags:
 *     - Reviews
 *     summary: Get a product's reviews
 *     description: >-
 *       Returns all the public reviews of a given product with pagination
 *     parameters:
 *       - $ref: '#/components/parameters/productIDPathParameter'
 *       - name: limit
 *         in: query
 *         description: the page size
 *         schema:
 *           type: string
 *       - name: page
 *         in: query
 *         description: the current page
 *         schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: Success
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/reviews_paginated'
 */
app.route("/products/:productID/reviews")
    .get(auth_middleware, function (req, res) {

        if (req.user) {
            req.query.authenticatedUser = req.user.userID;
            req.query.roles = req.user.roles.join();
        }

        req.query.product = req.productID;

        request({
            uri: reviewsService,
            qs: req.query,
            json: true,
            headers: { "Original-URL": api + req.originalUrl }
        }, function (err, resp, body) {
            if (err) {
                res.status(500).send();
            }
        }).pipe(res);
    });



// URL: /products/:productID/rating
//
// GET 		return the aggregated rating of a product
//

/**
 * @swagger
 * /products/{productID}/rating:
 *   get:
 *     tags:
 *     - Products
 *     summary: Get a product's aggregated rating
 *     description: >-
 *       Returns the aggregated rating of the product, based on all it's reviews
 *     parameters:
 *       - $ref: '#/components/parameters/productIDPathParameter'
 *     responses:
 *       '200':
 *         description: Success
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/productRating'
 */
app.route("/products/:productID/rating")
    .get(function (req, res) {
        request({
            uri: reviewsService + "/rating",
            qs: { product: req.productID },
            json: true,
            headers: { "Original-URL": api + req.originalUrl }
        }
            , function (err, resp, body) {
                if (err) {
                    res.status(500).send();
                }
            }).pipe(res);
    });


// URL: /users/{userID}/reviews
//
// GET return the reviews posted by the user. Only the user can see his reviews via this enpoint.
// optional params:
// - status -> one of [published,rejected,pending]
// Ex: /users/manel/reviews?status=pending
// 

/**
 * @swagger
 * /users/{userID}/reviews:
 *   get:
 *     tags:
 *     - Reviews
 *     summary: User reviews
 *     description: >-
 *       Returns all the reviews of a given user by status, with pagination
 *     parameters:
 *       - $ref: '#/components/parameters/userIDPathParameter'
 *       - name: status
 *         in: query
 *         description: the status of the 
 *         schema:
 *           type: string
 *       - name: page
 *         in: query
 *         description: the current page
 *         schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: Success
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               items:
 *                 $ref: '#/components/schemas/reviews'
*/
app.route("/users/:userID/reviews")
    .get(auth_middleware, function (req, res) {

        if (!req.user) {
            console.log("User not authenticated");
            res.status(401).send("Unauthorized");
            return;
        }

        request({
            uri: reviewsService,
            qs: {
                user: req.userID,
                authenticatedUser: req.user.userID,
                roles: req.user.roles,
                status: req.query.status
            },
            json: true,
            headers: { "Original-URL": api + req.originalUrl }
        },
            function (err, resp, body) {
                if (err) {
                    res.status(500).send();
                }
            }).pipe(res);
    });

const httpServer = http.createServer(app);
httpServer.listen(conf.apiPort, () => console.log("HTTP: Listening on " + conf.apiPort));
