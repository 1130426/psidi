const defaultPageSize = 20;

exports.paginatedResults = function (model, fieldName, page, limit) {
    if (!limit) {
        if (model.length < defaultPageSize) {
            limit = model.length;
        } else {
            limit = defaultPageSize;
        }
    }
    if (!page) {
        page = 1;
    }

    const startIndex = (page - 1) * limit
    const endIndex = page * limit

    const results = {}

    if (endIndex < model.length) {
        results.next = {
            page: page + 1,
            limit: limit
        }
    }

    if (startIndex > 0) {
        results.previous = {
            page: page - 1,
            limit: limit
        }
    }

    results[fieldName] = model.slice(startIndex, endIndex);
    return results;
};

exports.addLink = function (obj, rel, ref, templated) {

    if (!templated) {
        if (ref.constructor === Array) {
            obj[rel] = ref.map(e => ({ href: e }));
            return;
        }

        obj[rel] = { href: ref };
    } else {
        if (ref.constructor === Array) {
            obj[rel] = ref.map(e => ({ href: e, templated: templated }));
            return;
        }

        obj[rel] = { href: ref, templated: templated };
    }

}