const apiPort = 2999;
const apiPortSecure = 2998;
const productsPort = 3000;
const reviewsPort = 3001;
const usersPort = 3002;

exports.apiPort = apiPort;
exports.apiPortSecure = apiPortSecure;
exports.productsPort = productsPort;
exports.reviewsPort = reviewsPort;
exports.usersPort = usersPort;

const protocolPrefix = "http://";
const baseDomain = "localhost";

const baseUrl = protocolPrefix + baseDomain;

exports.apiURL = baseUrl + ":" + apiPort;
exports.productsURL = baseUrl + ":" + productsPort + "/products";
exports.reviewsURL = baseUrl + ":" + reviewsPort + "/reviews";
exports.usersURL = baseUrl + ":" + usersPort + "/users";
exports.linkRelPrefix = "psidi";
