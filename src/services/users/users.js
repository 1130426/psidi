const express = require('express');
var bodyParser = require('body-parser');
const funcs = require('../../funcs');
const conf = require('../../conf');
const port = conf.usersPort;

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// const bcrypt = require('bcrypt')
// const SALT = 0;

// SAMPLE DATA
var now = new Date();

// endpoints
var users = [
    { userID: "user" + 1, password: "1234", roles: ["customer"], createdOn: now, updatedOn: now },
    { userID: "user" + 2, password: "1234", roles: ["customer"], createdOn: now, updatedOn: now },
    { userID: "user" + 3, password: "1234", roles: ["customer"], createdOn: now, updatedOn: now },
    { userID: "user" + 4, password: "1234", roles: ["customer"], createdOn: now, updatedOn: now },
    { userID: "user" + 5, password: "1234", roles: ["mod"], createdOn: now, updatedOn: now },
    { userID: "user" + 6, password: "1234", roles: ["mod"], createdOn: now, updatedOn: now },
    { userID: "psidimenosde20ederrota@gmail.com", provider: "google", roles: ["mod", "customer"], createdOn: now, updatedOn: now }
];

// URL: /users
//
// post 		new user
// 
app.route("/users")
    .get(function (req, res) {
        res.json(funcs.paginatedResults(users, "users", parseInt(req.query.page), parseInt(req.query.limit)));
    });

app.param('userID', function (req, res, next, userID) {
    req.userID = userID;
    return next();
});


// URL: /users/:userID
//
// GET 		return 200 if credentials match
//
// PUT 		return 200 if user was created or updated
//
// get 		user
// put 		new user
// 
app.route("/users/:userID")
    .get(function (req, res) {

        console.log("---------- GET /users/" + req.userID + " ----------");
        console.log(req.userID);
        console.log(req.query);

        const user = users.find(u => 
            u.userID === req.userID && (!req.query.password && (req.query.provider === u.provider) 
            || (!req.query.provider && req.query.password === u.password))
        );

        if (user) {
            console.log("user " + req.userID + " exists.")
            res.json(user);
        } else {
            console.log("user " + req.userID + " does not exist.")
            res.status(404).send("User not found.");
        }
    })
    .put(function (req, res) {

        console.log("---------- PUT /users/:userID ----------");
        console.log(req.userID);
        console.log(req.body);

        const user = users.find(u => u.userID === req.userID);

        if (user) {
            res.status(400).send("Username already taken.");
        } else {
            let now = new Date();
            var newUser;
            if (req.body.password) {
                newUser = { userID: req.userID, password: req.body.password, roles: ["customer"], createdOn: now, updatedOn: now };
            } else {
                newUser = { userID: req.userID, provider: req.body.provider, roles: ["customer"], createdOn: now, updatedOn: now };
            }

            users.push(newUser);
            res.status(201).json(newUser);
            console.log("Registered new user: ");
            console.log(newUser);
        }
    });

app.listen(port, function () {
    console.log("Users service is listening on " + port);
});



// TODO: hash stuff?
// async function comparePassword(password, passwordToCompare) {
//     const salt = await bcrypt.genSalt(SALT);
//     const hash = await bcrypt.hash(password.toString(), salt);
//     const isSame = await bcrypt.compare(passwordToCompare.toString(), hash);
//     return isSame;
// }


// function hashPassword(password) {
//     const hash = bcrypt.hashSync(password,SALT);
//     return hash;
// }
