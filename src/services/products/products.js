
const express = require('express');
var bodyParser = require('body-parser');
const funcs = require('../../funcs');
const conf = require('../../conf');
const port = conf.productsPort;

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// SAMPLE DATA
var now = new Date();
var yesterday = now.getDate() - 1;
var jan1st2014 = new Date(2014, 01, 01);
var may2nd2014 = new Date(2014, 05, 02);

// DATA STORE
var products = [
    { id: "P1", version: 0, description: "Some description 1a ", details: { barcode: 10000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: now, updatedOn: now },
    { id: "P2", version: 0, description: "Some description 1b ", details: { barcode: 20000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: yesterday, updatedOn: now },
    { id: "P3", version: 0, description: "Some description 3  ", details: { barcode: 30000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: jan1st2014, updatedOn: now },
    { id: "P4", version: 0, description: "Some description 4  ", details: { barcode: 40000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
    { id: "P5", version: 0, description: "Some description 5  ", details: { barcode: 50000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
    { id: "P6", version: 0, description: "Some description 6  ", details: { barcode: 60000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
    { id: "P7", version: 0, description: "Some description 7  ", details: { barcode: 70000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
    { id: "P8", version: 0, description: "Some description 8  ", details: { barcode: 80000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
    { id: "P9", version: 0, description: "Some description 9  ", details: { barcode: 90000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
    { id: "P10", version: 0, description: "Some description 10", details: { barcode: 100000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
    { id: "P11", version: 0, description: "Some description 11", details: { barcode: 110000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
    { id: "P12", version: 0, description: "Some description 12", details: { barcode: 120000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
    { id: "P13", version: 0, description: "Some description 13", details: { barcode: 130000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
    { id: "P14", version: 0, description: "Some description 14", details: { barcode: 140000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
    { id: "P15", version: 0, description: "Some description 15", details: { barcode: 150000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
    { id: "P16", version: 0, description: "Some description 16", details: { barcode: 160000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
    { id: "P17", version: 0, description: "Some description 17", details: { barcode: 170000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
    { id: "P18", version: 0, description: "Some description 18", details: { barcode: 180000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
    { id: "P19", version: 0, description: "Some description 19", details: { barcode: 190000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
    { id: "P20", version: 0, description: "Some description 20", details: { barcode: 200000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
    { id: "P21", version: 0, description: "Some description 21", details: { barcode: 210000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
    { id: "P22", version: 0, description: "Some description 22", details: { barcode: 220000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
    { id: "P23", version: 0, description: "Some description 23", details: { barcode: 230000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
    { id: "P24", version: 0, description: "Some description 24", details: { barcode: 240000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
    { id: "P25", version: 0, description: "Some description 25", details: { barcode: 250000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
    { id: "P26", version: 0, description: "Some description 26", details: { barcode: 260000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
    { id: "P27", version: 0, description: "Some description 27", details: { barcode: 270000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
    { id: "P28", version: 0, description: "Some description 28", details: { barcode: 280000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
    { id: "P29", version: 0, description: "Some description 29", details: { barcode: 290000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
    { id: "P30", version: 0, description: "Some description 30", details: { barcode: 300000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
    { id: "P31", version: 0, description: "Some description 31", details: { barcode: 310000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
    { id: "P32", version: 0, description: "Some description 32", details: { barcode: 320000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
    { id: "P33", version: 0, description: "Some description 33", details: { barcode: 330000000, images: ["1", "2", "3"], sku: "barcode" }, createdOn: may2nd2014, updatedOn: now },
];


// URL: /products
//
// GET 		return all products with pagination
// pagination params:
// - limit -> a number
// - page -> a number
//
// other params:
// - barcode -> a number
// - productID -> an ID
//
// Ex: GET /products?page=2&limit=10&productID=abc&barcode=12345
//
app.route("/products")
    .get(function (req, res) {
        const matchingProducts = products.filter(
            p =>
                ((req.query.productID) ? p.id.startsWith(req.query.productID) : true) &&
                ((req.query.barcode) ? p.details.barcode.toString().startsWith(req.query.barcode) : true)
        );

        const paginated = funcs.paginatedResults(matchingProducts, "products", parseInt(req.query.page), parseInt(req.query.limit));

        var body = { _links: {}, _embedded: {} };

        funcs.addLink(body._links, "self", req.header("Original-URL"));
        body._links["curies"] = [{ "name": conf.linkRelPrefix, "href": conf.apiURL + "/docs" }];

        funcs.addLink(body._links, "find", conf.apiURL + "/products/{?id}", true);

        let strippedOriginalURL = req.header("Original-URL").split("?")[0];

        if (paginated.next) {
            funcs.addLink(body._links, "next", strippedOriginalURL + "?page=" + paginated.next.page + "&limit=" + paginated.next.limit);
        }

        if (paginated.previous) {
            funcs.addLink(body._links, "prev", strippedOriginalURL + "?page=" + paginated.previous.page + "&limit=" + paginated.previous.limit);
        }

        body._links["item"] = [];

        for (element of paginated.products) {
            var _elementLinks = productHALLinks(element);
            funcs.addLink(_elementLinks, "self", conf.apiURL + "/products/" + element.id);
            element["_links"] = _elementLinks;
            body._links["item"].push({ href: conf.apiURL + "/products/" + element.id });
        }

        body._embedded["item"] = paginated.products.map(p => ({ _links: p._links, id: p.id, description: p.description }));

        res.contentType("application/hal+json").status(200).json(body);
    });

app.param('productID', function (req, res, next, productID) {
    req.productID = productID;
    return next();
});

app.route("/products/:productID")
    .get(function (req, res) {
        const product = products.find(p => p.id === req.productID);
        if (product) {

            // if its a simple check and we don't want product info
            if (req.query.exists) {
                res.status(200).send();
                return;
            }

            var body = { _links: productHALLinks(product) };

            body.id = product.id;
            body.description = product.description;

            funcs.addLink(body._links, "self", req.header("Original-URL"));
            body._links["curies"] = [{ "name": conf.linkRelPrefix, "href": conf.apiURL + "/docs" }];

            res.status(200).json(body);
        } else {
            res.status(404).send("Product not found.");
        }
    });


// URL: /products/{productID}/sku
//
// GET 	return the SKU for this product
//
// Ex: GET /products/25/sku
//
app.route("/products/:productID/sku")
    .get(function (req, res) {
        const product = products.find(p => p.id === req.productID);
        if (product) {
            res.sendFile(product.details.sku + ".png", { root: __dirname + "/static/" });
        } else {
            res.status(404).send("Product not found.");
        }
    });

app.param('imageID', function (req, res, next, imageID) {
    req.imageID = imageID;
    return next();
});

// URL: /products/{productID}/images/{imageID}
//
// GET 		return the requested image for a product
// Ex: GET /products/25/images/1
//
app.route("/products/:productID/images/:imageID")
    .get(function (req, res) {
        const product = products.find(p => p.id === req.productID);
        if (product) {

            const image = product.details.images.find(i => i === req.imageID);
            if (image) {
                res.sendFile(req.imageID + ".png", { root: __dirname + "/static/" });
            } else {
                res.status(404).send("Image not found.");
            }
        } else {
            res.status(404).send("Product not found.");
        }
    });

app.listen(port, function () {
    console.log("Products service is listening on " + port);
});

function productHALLinks(product) {

    var _links = {};
    funcs.addLink(
        _links,
        conf.linkRelPrefix + ":image",
        product.details.images.map(i => conf.apiURL + "/products/" + product.id + "/images/" + i)
    );
    funcs.addLink(_links, conf.linkRelPrefix + ":sku", conf.apiURL + "/products/" + product.id + "/sku");
    funcs.addLink(_links, conf.linkRelPrefix + ":reviews", conf.apiURL + "/products/" + product.id + "/reviews");
    funcs.addLink(_links, conf.linkRelPrefix + ":rating", conf.apiURL + "/products/" + product.id + "/rating");

    return _links;
}