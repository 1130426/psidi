const express = require('express');
var bodyParser = require('body-parser');

const util = require('util')
const request = require("request");
const requestPromise = util.promisify(request);
const funcs = require('../../funcs');
const conf = require('../../conf');
const { authenticate } = require('passport');
const { exception } = require('console');
const port = conf.reviewsPort;

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const funFactServerURL = "http://numbersapi.com";

// SAMPLE .DATA
var now = new Date();
var yesterday = new Date();
var beforeYesterday = new Date();
var longAgo = new Date();
var longerAgo = new Date();
yesterday.setDate(now.getDate() - 1);
beforeYesterday.setDate(now.getDate() - 2);
longAgo.setDate(now.getDate() - 10);
longerAgo.setDate(now.getDate() - 45);

// DATA STORE
var nextID = 14;
var reviews = [
    { id: "R1", username: "user1", review: "Some .descriptionzz here1", votes: "9", product: "P1", rating: "5", status: "pending", createdOn: now, updatedOn: now },
    { id: "R2", username: "user2", review: "Some .descriptionzz here2", votes: "0", product: "P2", rating: "3.5", status: "pending", createdOn: now, updatedOn: now },
    { id: "R3", username: "user2", review: "Some .descriptionzz here3", votes: "0", product: "P1", rating: "3.5", status: "pending", createdOn: now, updatedOn: now },
    { id: "R4", username: "user2", review: "Some .descriptionzz here4", votes: "0", product: "P3", rating: "3.5", status: "published", publishingDate: yesterday, createdOn: now, updatedOn: now },
    { id: "R5", username: "user1", review: "Some .descriptionzz here5", votes: "0", product: "P1", rating: "3.5", status: "published", publishingDate: yesterday, createdOn: now, updatedOn: now },
    { id: "R6", username: "user1", review: "Some .descriptionzz here6", votes: "0", product: "P2", rating: "3.5", status: "pending", createdOn: now, updatedOn: now },
    { id: "R7", username: "user1", review: "Some .descriptionzz here7", votes: "2", product: "P5", rating: "3.5", status: "published", createdOn: now, updatedOn: now },
    { id: "R8", username: "user2", review: "Some .descriptionzz here8", votes: "0", product: "P3", rating: "3.5", status: "rejected", publishingDate: yesterday, createdOn: now, updatedOn: now },
    { id: "R9", username: "user2", review: "Some .descriptionzz here9", votes: "0", product: "P3", rating: "3.5", status: "pending", createdOn: now, updatedOn: now },
    { id: "R10", username: "user2", review: "Some .descriptionzz here10", votes: "0", product: "P1", rating: "3.5", status: "published", publishingDate: longAgo, createdOn: longAgo, updatedOn: longAgo },
    { id: "R11", username: "user3", review: "MMMMMM", votes: "0", product: "P1", rating: "3.5", status: "published", publishingDate: beforeYesterday, createdOn: longAgo, updatedOn: longAgo },
    { id: "R12", username: "user4", review: "I really don't like this product. i Mean REalLyy", votes: "0", product: "P1", rating: "1.5", status: "published", publishingDate: longerAgo, createdOn: longerAgo, updatedOn: longerAgo },
    { id: "R13", username: "user3", review: ":D", votes: "0", product: "P47", rating: "1.5", status: "pending", publishingDate: longerAgo, createdOn: longerAgo, updatedOn: longerAgo },
]

var reviewTracker = [
    { review: "R1", etag: '1' },
    { review: "R2", etag: '2' },
    { review: "R3", etag: '1' },
    { review: "R4", etag: '1' },
    { review: "R5", etag: '1' },
    { review: "R6", etag: '1' },
    { review: "R7", etag: '1' },
    { review: "R8", etag: '1' },
    { review: "R9", etag: '1' },
    { review: "R10", etag: '1' },
    { review: "R11", etag: '1' },
    { review: "R12", etag: '1' },
    { review: "R13", etag: '1' },
]


app.route("/reviews")
    // ?
    // status=pending/reported/rejected
    // user=user50 
    // product=P4
    // authenticatedUser=user2
    .get(function (req, res) {

        var moderator = false;

        if (req.query.authenticatedUser) {
            console.log("Is authenticated");
            if (req.query.roles.includes("mod")) {
                console.log("Is moderator");
                moderator = true;
            }

            // trying to see non-published reviews of other users
            if (req.query.user && req.query.status !== "published" && req.query.user !== req.query.authenticatedUser && !moderator) {
                res.status(401).send();
                return;
            }
        } else {
            // trying to see non-published reviews as a non-authenticated user
            if (req.query.status && req.query.status !== "published") {
                res.status(401).send();
                return;
            }
            req.query.status = "published";
        }

        if (req.query.product) {
            req.query.status = "published";
        }

        const reviewsToReturn = reviews.filter(r =>
            (req.query.status ? r.status === req.query.status : true) &&
            (req.query.product ? r.product === req.query.product : true) &&
            (req.query.user ? r.username === req.query.user : true)
        )
            .sort((ra, rb) => rb.votes - ra.votes)
            .sort((ra, rb) => rb.publishingDate - ra.publishingDate);

        const paginated = funcs.paginatedResults(
            reviewsToReturn,
            "reviews",
            parseInt(req.query.page),
            parseInt(req.query.limit)
        );

        var body = { _links: {}, _embedded: {} };

        funcs.addLink(body._links, "self", req.header("Original-URL"));
        body._links["curies"] = [{ "name": conf.linkRelPrefix, "href": conf.apiURL + "/docs" }];

        funcs.addLink(body._links, "find", conf.apiURL + "/reviews/{?id}", true);

        let strippedOriginalURL = req.header("Original-URL").split("?")[0];

        if (paginated.next) {
            funcs.addLink(body._links, "next", strippedOriginalURL + "?page=" + paginated.next.page + "&limit=" + paginated.next.limit);
        }

        if (paginated.previous) {
            funcs.addLink(body._links, "prev", strippedOriginalURL + "?page=" + paginated.previous.page + "&limit=" + paginated.previous.limit);
        }

        body._links["item"] = [];

        for (element of paginated.reviews) {
            var _elementLinks = reviewHALLinks(element, req.query.authenticatedUser, moderator);
            funcs.addLink(_elementLinks, "self", conf.apiURL + "/reviews/" + element.id);
            element["_links"] = _elementLinks;
            body._links["item"].push({ href: conf.apiURL + "/reviews/" + element.id });
        }

        body._embedded["item"] = paginated.reviews;

        res.contentType("application/hal+json").status(200).json(body);
    })
    // Since this is meant to be asynchronous, the reviewID will be used by the user to check its details later.    
    .post(function (req, res) {

        let product = req.body.product;
        let review = req.body.description;
        let rating = req.body.rating;
        let username = req.body.user;

        if (username === undefined) {
            res.status(400).send("Missing username");
        } else if (product === undefined || review === undefined || rating === undefined) {
            res.status(400).send("Missing body parameters");
        } else if (validateRating(rating) === null) {
            res.status(400).send("Invalid rating. The rating must be a number between 0 and 5, with half ratings included.")
        } else {

            // check product exists
            request({
                uri: conf.productsURL + "/" + product,
                qs: { exists: true }
            },
                function (err, resp, body) {
                    if (resp.statusCode == "200") {
                        let reviewID = generateNewID();
                        res.status(202)
                            .location(conf.apiURL + "/reviews/" + reviewID)
                            .send();

                        let atm = new Date();
                        let newReview = {
                            id: reviewID,
                            username: username,
                            review: review,
                            votes: "0",
                            product: product,
                            rating: rating,
                            status: "pending",
                            createdOn: atm,
                            updatedOn: atm
                        };
                        reviews.push(newReview);

                    } else {
                        res.status(404).send("Product does not exist.");
                    }
                });
        }
    });

app.route("/reviews/rating")
    .get(function (req, res) {

        if (!req.query.product) {
            res.status(400).send("Must specify product by parameter");
        }

        const productReviews = reviews.filter(r => r.product === req.query.product);

        let stars = {
            "0": 0, "0.5": 0,
            "1": 0, "1.5": 0,
            "2": 0, "2.5": 0,
            "3": 0, "3.5": 0,
            "4": 0, "4.5": 0,
            "5": 0,
        };

        for (var i = 0; i < productReviews.length; i++) {
            stars[productReviews[i].rating] += 1;
        }

        let sum = 0;
        for (const [key, value] of Object.entries(stars)) {
            sum += Number(key) * value;
        }

        let body = {
            aggregatedRating: Number((sum / productReviews.length).toFixed(1)),
            reviews: productReviews.length,
            stars: stars
        }

        body._links = {};
        funcs.addLink(body._links, "self", req.header("Original-URL"));
        // body._links["curies"] = [{ name: conf.linkRelPrefix, "href": conf.apiURL + "/docs" }];

        res.status(200).json(body);
    });


app.param('reviewID', function (req, res, next, reviewID) {
    req.reviewID = reviewID;
    return next();
});

app.route("/reviews/:reviewID")
    .get(function (req, res) {
        const review = reviews.find(r => r.id === req.reviewID);
        if (review) {
            let currentEtag = getEtag(req.reviewID);
            res.header("etag", currentEtag);

            if (!req.query.authenticatedUser && review.status !== "published") {
                res.status(404).send("Review not found.");
                return;
            }

            if (req.query.authenticatedUser !== review.username && !req.query.roles.includes("mod")) {
                res.status(404).send("Review not found.");
                return;
            }

            review._links = reviewHALLinks(
                review,
                req.query.authenticatedUser,
                req.query.authenticatedUser && req.query.roles.includes("mod")
            );

            funcs.addLink(review._links, "self", req.header("Original-URL"));
            review._links["curies"] = [{ name: conf.linkRelPrefix, "href": conf.apiURL + "/docs" }];

            res.contentType("application/hal+json").status(200).json(review);
        } else {
            res.status(404).send("Review not found.");
        }
    })
    .delete(async function (req, res) {
        let rev = req.header("if-match");
        let currentEtag = getEtag(req.reviewID);
        if (rev === undefined) {
            res.status(400).send("Missing header 'if-match'");
            console.log("message text was NOT updated since the request had no etag");
        } else if (rev == currentEtag) {
            const review = reviews.find(r => r.id === req.reviewID);
            if (!review) {
                console.log("Review not found");
                res.status(404).send("Review not found.");
            } else {
                if (req.query.authenticatedUser === review.username
                    || req.query.roles.includes("mod")) {   // if user owns the data or is a moderator

                    let votes = review.votes;

                    if (votes != undefined && parseInt(review.votes) === 0) {
                        let reviewIndex = reviews.indexOf(review);
                        reviews.splice(reviewIndex, 1);
                        res.status(200).send();
                    } else {
                        res.status(412).send("The review you are trying to delete has votes already. Therefore, it cannot be deleted.");
                    }
                } else {
                    res.status(403).send("You can't delete a review you don't own.");
                }

            }
        } else {
            res.status(400).send("Etag out-of-date. Please pull the latest review and update the etag accordingly");
        }
    })
    .post(async function (req, res) {
        let action = req.body.action;
        let rev = req.header("if-match");
        let currentEtag = getEtag(req.reviewID);
        if (!action) {
            res.status(400).send("An action must be provided.");
        } else if (action != "approve" && action != "reject" && action != "report" && action != "vote") {
            res.status(400).send("You provided an invalid action.");
        } else if (rev === undefined) {
            res.status(400).send("Missing header 'if-match'");
            console.log("message text was NOT updated since the request had no etag");
        } else if (rev == currentEtag) {
            const review = reviews.find(r => r.id === req.reviewID);
            if (!review) {
                console.log("Review not found");
                res.status(404).send("Review not found.");
            } else {

                if (req.query.roles.includes("mod") && (action === "approve" || action === "reject")) {
                    /*
                     * MODERATOR
                     * can APPROVE or REJECT a review
                     */
                    if (review.status != "pending" && review.status != "reported") {
                        res.status(400).send("Invalid review status. Must be in \"pending\" state.");
                    } else {
                        await approveReview(req, res, action, review);
                    }
                } else if (req.query.roles.includes("customer") && (action === "report" || action === "vote")) {
                    /* 
                     * CUSTOMER
                     * can VOTE or REPORT a review
                     */
                    if (review.status != "published") {
                        res.status(400).send("Invalid review status. You can only report public reviews.");
                    } else {
                        if (action === "report") {
                            await reportReview(req, res, review);
                        } else if (action === "vote") {
                            await voteReview(req, res, review);
                        }
                    }
                } else {
                    res.status(403).send("Insuficient action rights");
                }

            }
        } else {
            res.status(400).send("Etag out-of-date. Please pull the latest review and update the etag accordingly");
        }
    });

async function approveReview(req, res, action, review) {

    console.log("headers: " + req.headers);
    console.log("body: " + req.body);
    console.log("qs: " + req.query);

    var index = reviews.indexOf(review);
    reviews[index].updatedOn = new Date();
    let now = new Date();
    if (action === "approve") {
        let month = now.getMonth();
        let day = now.getDay();
        const funFact = await requestPromise({ uri: funFactServerURL + "/" + month + "/" + day + "/date", json: true });
        reviews[index].fun_fact = funFact.body;
        reviews[index].publishingDate = now;
        reviews[index].status = "published";
    } else {
        reviews[index].status = "rejected";
    }

    let newEtag = increaseEtag(review.id);
    res.header("etag", newEtag);

    reviews[index]._links = reviewHALLinks(
        reviews[index],
        req.query.authenticatedUser,
        req.query.authenticatedUser && req.query.roles.includes("mod")
    );

    funcs.addLink(reviews[index]._links, "self", req.header("Original-URL"));
    reviews[index]._links["curies"] = [{ name: conf.linkRelPrefix, "href": conf.apiURL + "/docs" }];

    res.status(200).json(reviews[index]);
}

async function voteReview(req, res, review) {

    console.log("headers: " + req.headers);
    console.log("body: " + req.body);
    console.log("qs: " + req.query);

    var index = reviews.indexOf(review);
    if (review.votes) {
        let votesInteger = parseInt(review.votes);
        votesInteger++;
        reviews[index].votes = "" + votesInteger;
    } else {
        reviews[index].votes = "1";
    }

    reviews[index].updatedOn = new Date();

    let newEtag = increaseEtag(review.id);
    res.header("etag", newEtag);

    reviews[index]._links = reviewHALLinks(
        reviews[index],
        req.query.authenticatedUser,
        req.query.authenticatedUser && req.query.roles.includes("mod")
    );

    funcs.addLink(reviews[index]._links, "self", req.header("Original-URL"));
    reviews[index]._links["curies"] = [{ name: conf.linkRelPrefix, "href": conf.apiURL + "/docs" }];

    res.status(200).json(reviews[index]);
}

async function reportReview(req, res, review) {

    console.log("headers: " + req.headers);
    console.log("body: " + req.body);
    console.log("qs: " + req.query);

    var index = reviews.indexOf(review);
    reviews[index].updatedOn = new Date();
    reviews[index].status = "reported";

    let newEtag = increaseEtag(review.id);
    res.header("etag", newEtag);

    reviews[index]._links = reviewHALLinks(
        reviews[index],
        req.query.authenticatedUser,
        req.query.authenticatedUser && req.query.roles.includes("mod")
    );

    funcs.addLink(reviews[index]._links, "self", req.header("Original-URL"));
    reviews[index]._links["curies"] = [{ name: conf.linkRelPrefix, "href": conf.apiURL + "/docs" }];
    res.status(200).json(reviews[index]);
}

function validateRating(rating) {
    if (isNaN(rating)) {
        console.log("ERROR: " + rating + " is not a number");
        return null;
    } else if (rating > 5 || rating < 0 || rating % 0.5 != 0) {
        console.log("ERROR: The rating must be a number between 0 and 5, with half ratings included.");
        return null;
    } else {
        return 'OK';
    }
}

function generateNewID() {
    let firstHalf = "R";
    let secondHalf = nextID;
    let newID = firstHalf.concat(secondHalf);

    nextID++;

    return newID;
}

function getEtag(reviewID) {
    const review = reviewTracker.find(rv => rv.review === reviewID);
    return review ? review.etag : null;
}

function increaseEtag(reviewID) {
    const review = reviewTracker.find(rv => rv.review === reviewID);

    if (review) {
        let tag = parseInt(review.etag);
        tag++;
        review.etag = "" + tag;
        return review.etag;
    } else if (reviews.find(r => r.id === reviewID)) {
        reviewTracker.push({ review: reviewID, etag: '1' });
        return '1';
    }
}

function reviewHALLinks(review, authenticatedUser, moderator) {

    delete review._links;

    console.log(authenticatedUser);
    console.log(moderator);

    var _links = {};

    funcs.addLink(_links, conf.linkRelPrefix + ":product", conf.apiURL + "/products/" + review.product);

    var vote = false;
    var report = false;
    var withdraw = false;
    var approve = false;
    var reject = false;

    if (authenticatedUser) {
        vote = true;
        report = true;
    }

    if (authenticatedUser === review.username) {
        if (review.votes == 0) {
            withdraw = true;
        }
        vote = false;
        report = false;
    }

    if (moderator) {
        switch (review.status) {
            case "pending":
            case "reported":
                approve = true;
                reject = true;
                break;
            default:
                console.log("moderator no actions for review " + review.id + " with status " + review.status);
        }

        vote = false;
        report = false;
    }

    if (review.status === "reported") {
        report = false;
        vote = false;
    }

    if (vote) {
        funcs.addLink(_links, conf.linkRelPrefix + ":vote", conf.apiURL + "/reviews/" + review.id);
    }
    if (report) {
        funcs.addLink(_links, conf.linkRelPrefix + ":report", conf.apiURL + "/reviews/" + review.id);
    }
    if (withdraw) {
        funcs.addLink(_links, conf.linkRelPrefix + ":delete", conf.apiURL + "/reviews/" + review.id);
    }
    if (approve) {
        funcs.addLink(_links, conf.linkRelPrefix + ":approve", conf.apiURL + "/reviews/" + review.id);
    }
    if (reject) {
        funcs.addLink(_links, conf.linkRelPrefix + ":reject", conf.apiURL + "/reviews/" + review.id);
    }

    return _links;
}

app.listen(port, function () {
    console.log("Reviews service is listening on " + port);
});